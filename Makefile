# Собрать все api-компоненты
build-all: build-manager-api

# Собрать GRPC для handler
build-handler-api:
	protoc --go_out=../handler/src/api \
	   --go-grpc_out=../handler/src/api \
	   src/rpc/manager.proto

# Собрать GRPC для manager
build-manager-api:
	protoc --go_out=src/api \
	   --go-grpc_out=src/api \
	   src/rpc/manager.proto

# Собрать все GRPC
build-grcp: build-manager-api build-handler-api

# Очистить все
clean-all:
	rm -rf src/api/proto


# Подгрузить ENV для локальной отладки
load-debug-env:
	sh test/debug_env

# Запусить сервер
run-manager: 
	cd src && go run ./

# Запусить evans
run-evans:
	evans src/rpc/manager.proto

# Запусить БД
run-mongo-pod:
	podman pod start gabber-mongo 
	

.PHONY: \
	build-all \
	build-manager \
	clean-all
