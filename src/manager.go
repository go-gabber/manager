package main

import (
	rpc_handler "gabber/manager/api"
	"os"
)

func main() {
	var RPCServer rpc_handler.RPCHandler = rpc_handler.RPCHandler{}
	// Подключаемся к БД
	RPCServer.ConnectToDB(os.Getenv("GABBER_DB"))
	// Подключаемся к очереди
	RPCServer.ConnectToQueue(os.Getenv("GABBER_QUEUE"))
	// Стартуем сервер
	RPCServer.Connect(":" + os.Getenv("GRPC_PORT"))
}
