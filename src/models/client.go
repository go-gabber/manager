package gabber_models

type Client struct {
	ID         string
	FCDate     int64
	Permissons *Permissions
}

type Permissions struct {
	SendSMS    bool
	ReadSMS    bool
	FileAccess bool
}

type SMS struct {
	Origin  string
	Date    int
	Message string
}

type ActionModel struct {
	DownloadFile *File
	UploadFile   *File

	GetPermissions *Permissions
	SendSMS        *SMS
	GetAllSMS      []*SMS
}

type File struct {
	Source      string
	Destination string
}
