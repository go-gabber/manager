package rpc_handler

import (
	"context"
	"log"
	"net"
	"time"

	manager "gabber/manager/api/proto"
	db_interface "gabber/manager/db"

	"google.golang.org/grpc"
)

type RPCHandler struct {
	manager.UnimplementedManagerServer
	server *grpc.Server
	db     db_interface.DBProvider
}

func (s *RPCHandler) ConnectToDB(db string) {
	s.db = db_interface.GetDBHandler(db)
	s.db.Connect()
}

func (s *RPCHandler) ConnectToQueue(queue string) {
	// To be cUmming
}

func (s *RPCHandler) Connect(managerPort string) {
	lis, err := net.Listen("tcp", managerPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s.server = grpc.NewServer()
	manager.RegisterManagerServer(s.server, s)

	if err := s.server.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

	log.Println("RPC server started !")
}

func (s *RPCHandler) OnClientConnection(ctx context.Context, info *manager.AuthInfo) (*manager.Client, error) {
	var client = &manager.Client{
		ID:     info.SN,
		FCDate: time.Now().Unix(),
		Permissions: &manager.Permissions{
			SendSMS:    false,
			ReadSMS:    false,
			FileAccess: false,
		}}
	s.db.AddClient(client)
	return client, nil
}

func (s *RPCHandler) UpdateClientInfo(ctx context.Context, client *manager.Client) (*manager.RequestStatus, error) {
	s.db.UpdateClient(client)
	return &manager.RequestStatus{IsOK: true}, nil
}

func (s *RPCHandler) GetAction(context.Context, *manager.Client) (*manager.Action, error) {

	return &manager.Action{}, nil
}

func (s *RPCHandler) OnNewSMS(ctx context.Context, sms *manager.NewSMS) (*manager.RequestStatus, error) {
	s.db.AddSMS(sms)
	return &manager.RequestStatus{IsOK: true}, nil
}
