package gabber_mongo

import (
	"context"
	"fmt"

	manager "gabber/manager/api/proto"

	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type clientMongoDoc struct {
	Client *manager.Client
}

type message struct {
	*manager.SMS
}

type MongoCreds struct {
	Host     string
	Port     string
	Password string
	User     string
	Db       string
}

type collections struct {
	clients *mongo.Collection
}

type Provider struct {
	connection *mongo.Client
	ctx        context.Context
	creds      MongoCreds
	collections
}

func (p *Provider) loadENV() {
	p.creds = MongoCreds{
		Host:     os.Getenv("MONGO_HOST"),
		Port:     os.Getenv("MONGO_PORT"),
		User:     os.Getenv("MONGO_USER"),
		Password: os.Getenv("MONGO_PASSWORD"),
		Db:       os.Getenv("MONGO_DB"),
	}
}

func (p *Provider) Connect() error {
	p.loadENV()
	var cancel context.CancelFunc
	p.ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var err error
	p.connection, err = mongo.Connect(p.ctx, options.Client().ApplyURI(
		"mongodb://"+p.creds.User+
			":"+p.creds.Password+
			"@"+p.creds.Host+
			":"+p.creds.Port,
	))

	if err != nil {
		log.Println("DB connecton error: ", err)
		return err
	}

	p.collections.clients = p.connection.Database(p.creds.Db).Collection("clients")

	log.Println("DB connection: success ! ")
	return nil
}

func (p *Provider) Disconnect() error {
	return nil
}

func (p *Provider) AddSMS(sms *manager.NewSMS) error {
	p.push(p.collections.clients, sms.SMS, sms.Client)
	return nil
}

func (p *Provider) GetSMS() (sms *manager.SMS, err error) {
	err = nil
	return
}

func (p *Provider) AddClient(client *manager.Client) error {
	p.create(p.collections.clients, &clientMongoDoc{
		Client: client,
	})
	return nil
}

func (p *Provider) UpdateClient(client *manager.Client) error {
	return nil
}

func (p *Provider) GetClient(id string) (client *manager.Client, err error) {
	err = nil
	return
}

func (p Provider) structToDoc(object interface{}) (document bson.D, err error) {
	log.Println("STD: ", object)
	data, err := bson.Marshal(object)
	if err != nil {
		log.Println("Mongo - structToDoc ERROR! : ", err)
		return nil, err
	}
	log.Println("MD: ", data)
	err = bson.Unmarshal(data, &document)
	if err != nil {
		log.Println("UMN ERROR ! :", err)
	}
	return
}

func (p *Provider) create(collection *mongo.Collection, object interface{}) (objectID string, err error) {
	newDocument, err := p.structToDoc(object)
	if err != nil {
		return "", err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	res, err := collection.InsertOne(ctx, newDocument)
	if err != nil {
		log.Println("Mongo - create error! : ", err)
		return "", err
	}

	log.Println("Mongo - new document: ", res.InsertedID)

	objectID = fmt.Sprintln(res.InsertedID)
	err = nil
	return
}

func (p *Provider) read(collection *mongo.Collection, objectID string, object interface{}) (err error) {
	filter := bson.D{}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Add error
	collection.FindOne(ctx, filter).Decode(object)

	err = nil
	return
}

func (p *Provider) update(collection *mongo.Collection, object interface{}, objectID string) error {
	newDocument, err := p.structToDoc(object)
	if err != nil {
		return err
	}

	log.Println(newDocument)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	objID, err := primitive.ObjectIDFromHex(objectID)

	filter := bson.M{"_id": bson.M{"$eq": objID}}
	update := bson.M{"$setOnInsert": newDocument}

	res, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		log.Println("Mongo - update error : ", err)
		return err
	}
	log.Println(res.UpsertedID)
	return nil
}

func (p *Provider) push(collection *mongo.Collection, object interface{}, objectID string) error {
	newDocument, err := p.structToDoc(object)
	if err != nil {
		return err
	}

	log.Println(newDocument)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	filter := bson.M{"client.id": bson.M{"$eq": objectID}}
	update := bson.M{"$push": bson.M{"client.messages": newDocument}}

	res, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		log.Println("Mongo - update error : ", err)
		return err
	}
	log.Println(res.UpsertedID)
	return nil
}
