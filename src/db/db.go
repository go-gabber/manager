package db_interface

import (
	manager "gabber/manager/api/proto"
	gabber_mongo "gabber/manager/db/mongo"
)

type DBProvider interface {
	Connect() error
	Disconnect() error
	AddSMS(sms *manager.NewSMS) error
	AddClient(client *manager.Client) error
	UpdateClient(client *manager.Client) error
	GetClient(id string) (client *manager.Client, err error)
}

func GetDBHandler(handlerName string) DBProvider {
	switch handlerName {
	case "mongo-v1":
		return &gabber_mongo.Provider{}
	default:
		return &gabber_mongo.Provider{}
	}
}
